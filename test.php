<?php
function test($str)
{
    $two = ['()', '[]', '{}'];
    $open = '';
    $close = '';
    $check = '';
    for ($i = 0, $s = sizeof($two); $i < $s; $i++) {
        $open .= $two[$i][0];
        $close .= $two[$i][1];
        $check = preg_quote($open.$close, '/');
    }
//    if(!preg_match("/^[{$check}]+$/", $str)) {
//        echo "input error\n";
//        return;
//    }

    $ar = [];
    for ($i = 0, $s = strlen($str); $i < $s; $i++) {
        if (strpos($open, $str[$i]) !== false) {
            array_push($ar, $str[$i]);
        }
        if (strpos($close, $str[$i]) !== false) {
            if($ar == []) {
                echo "непарная закрывающая скобка";
                return;
            }
            $t = array_pop($ar);
            if(!in_array($t.$str[$i], $two)) {
                echo "непарная закрывающая скобка";
                return;
            }
        }
    }

    if ($ar != []) {
        echo "непарная открывающая скобка";
    } else {
        echo "ok";
    }


}

test('{}()([]({})[])');